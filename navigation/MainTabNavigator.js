import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import CalculatorScreen from '../screens/CalculatorScreen';


const CalculatorStack = createStackNavigator({
  Calculator: CalculatorScreen,
});

CalculatorStack.navigationOptions = {
  tabBarLabel: 'Calculator',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-calculator'}
    />
  ),
};


export default createBottomTabNavigator({
  CalculatorStack,
});
